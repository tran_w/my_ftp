/*
** loop.c for  in /home/tran_w/Projects/my_ftp/client
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Wed Apr 10 14:55:26 2013 tran_w
** Last update Sun Apr 14 22:12:47 2013 tran_w
*/

#include	<sys/select.h>
#include	<sys/time.h>
#include	<sys/types.h>
#include	<string.h>
#include	<unistd.h>
#include	<stdio.h>
#include	"client.h"

static int	read_server(const int sock, char *buffer)
{
  if ((my_recv(sock, buffer, BUFFER_SIZE)) == 0 ||
      !strcmp(DISCONNECT_MSG, buffer))
    {
      puts(KICKED_MSG);
      return (R_FAILURE);
    }
  return (R_SUCCESS);
}

static int	read_input(char *buffer)
{
  char		*pos_eol;

  if (fgets(buffer, BUFFER_SIZE, stdin))
  {
    if ((pos_eol = strstr(buffer, EOL)))
      buffer[pos_eol - buffer] = 0;
    return (R_SUCCESS);
  }
  return (R_FAILURE);
}

static void	set_fds(const int sock, fd_set *rfds, fd_set *wfds)
{
  FD_ZERO(rfds);
  FD_ZERO(wfds);
  FD_SET(STDIN_FILENO, rfds);
  FD_SET(sock, rfds);
  FD_SET(sock, wfds);
}

static void	show_prompt()
{
  unsigned	i;

  i = 0;
  while (PROMPT[i])
    write(STDIN_FILENO, &PROMPT[i++], 1);
}

int		loop(const int sock)
{
  char		buffer[BUFFER_SIZE];
  fd_set       	rfds;
  fd_set	wfds;

  show_prompt();
  while (IS_TRUE)
    {
      set_fds(sock, &rfds, &wfds);
      if (select(sock + 1, &rfds, &wfds, NULL, NULL) == R_FAILURE)
	return (R_FAILURE);
      if (FD_ISSET(STDIN_FILENO, &rfds) && FD_ISSET(sock, &wfds))
	{
	  read_input(buffer);
	  my_send(sock, buffer, strlen(buffer));
	}
      else if (FD_ISSET(sock, &rfds))
	{
	  if (read_server(sock, buffer))
	    return (R_SUCCESS);
	  puts(buffer);
	}
    }
  return (R_SUCCESS);
}

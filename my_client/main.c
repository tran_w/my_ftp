/*
** main.c for  in /home/tran_w/Projects/my_ftp/client
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr  8 13:20:06 2013 tran_w
** Last update Sun Apr 14 19:24:01 2013 tran_w
*/

#include	<stdlib.h>
#include	<stdio.h>
#include	<string.h>
#include	"client.h"

static void	hello_server(const t_client *client)
{
  if (client->ip)
    my_send(client->sock, client->ip, strlen(client->ip));
  else
    my_send(client->sock, "Hello", 5);
}

int		main(int ac, char **av)
{
  t_client	client;

  if (ac != 3)
    {
      usage();
      return (EXIT_FAILURE);
    }
  if ((init_connection(&client, av[1], atoi(av[2]))) == R_FAILURE)
    {
      fprintf(stderr, "%s\n%s\n", ERR_INIT, END_OF_PROGRAM);
      return (EXIT_FAILURE);
    }
  hello_server(&client);
  if (loop(client.sock) == R_FAILURE)
    {
      fprintf(stderr, "%s\n%s\n", ERR_SELECT, END_OF_PROGRAM);
      close(client.sock);
      return (EXIT_FAILURE);
    }
  close(client.sock);
  puts(GOODBYE_MSG);
  return (EXIT_SUCCESS);
}

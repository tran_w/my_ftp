/*
** my_recv.c for  in /home/tran_w/Projects/my_ftp/server
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr  8 21:35:49 2013 tran_w
** Last update Thu Apr 11 13:20:20 2013 tran_w
*/

#include	<unistd.h>
#include	<stdio.h>
#include	"client.h"

size_t		my_recv(int socket, char *buffer, size_t len)
{
  int		byte;

  if ((byte = read(socket, buffer, len)) == R_FAILURE)
    {
      fprintf(stderr, "%s\n", ERR_RECV);
      return (R_FAILURE);
    }
  buffer[byte] = '\0';
  return (byte);
}

/*
** init_connection.c for  in /home/tran_w/Projects/my_ftp/server
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr  8 16:46:40 2013 tran_w
** Last update Sat Apr 13 13:09:50 2013 tran_w
*/

#include		<stdlib.h>
#include		<arpa/inet.h>
#include		<sys/types.h>
#include		<sys/socket.h>
#include		<netdb.h>
#include		<stdio.h>
#include		"client.h"

static int		init_sock(t_client *client,
				    struct sockaddr_in *s_in,
				    struct protoent *pe,
				    const int port)
{
  if ((client->sock = socket(AF_INET,
			       SOCK_STREAM, pe->p_proto)) == R_FAILURE)
    {
      fprintf(stderr, "%s\n", ERR_SOCKET);
      return (R_FAILURE);
    }
  s_in->sin_family = AF_INET;
  s_in->sin_port = htons(port);
  return (R_SUCCESS);
}

static int		init_protocol(struct protoent *pe)
{
  if ((pe = getprotobyname(PROTOCOL)) == NULL)
    {
      fprintf(stderr, "%s\n", ERR_PROTOCOL);
      return (R_FAILURE);
    }
  return (R_SUCCESS);
}

int		 	init_connection(t_client *client,
					const char *address,
					const int port)
{
  struct protoent	pe;
  struct sockaddr_in	s_in;

  if (init_protocol(&pe))
    return (R_FAILURE);
  if (init_sock(client, &s_in, &pe, port))
    return (R_FAILURE);
  s_in.sin_addr.s_addr = inet_addr(address);
  if (connect(client->sock, (const struct sockaddr *)&s_in, sizeof(s_in)))
    {
      fprintf(stderr, "%s\n", ERR_CONNECT);
      close(client->sock);
      return (R_FAILURE);
    }
  client->ip = inet_ntoa(s_in.sin_addr);
  return (R_SUCCESS);
}

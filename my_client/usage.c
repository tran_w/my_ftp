/*
** usage.c for  in /home/tran_w/Projects/my_ftp/server
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr  8 16:25:44 2013 tran_w
** Last update Wed Apr 10 11:50:28 2013 tran_w
*/

#include	<stdio.h>
#include	"client.h"

void		usage(void)
{
  printf("How to use my_client:\n");
  printf("%s\n", CLIENT_USAGE);
}

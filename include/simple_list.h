/*
** simple_list.h for  in /home/tran_w/Projects/my_ftp/include
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Fri Apr 12 16:36:21 2013 tran_w
** Last update Sun Apr 14 16:24:14 2013 tran_w
*/

#ifndef			SIMPLE_LIST_H_
# define		SIMPLE_LIST_H_

/*
** Types
*/

typedef enum		e_bool
{
    FALSE,
    TRUE
}			t_bool;

typedef struct		s_node
{
  int			sock;
  struct s_node		*next;
}			t_node;

typedef t_node		*t_list;


/*
** Functions
*/

/* Informations */

unsigned int		list_get_size(t_list list);
t_bool			list_is_empty(t_list list);
void			list_dump(t_list list);

/* Modification */

t_bool			list_add_elem_at_front(t_list *front_ptr, int elem);
t_bool			list_add_elem_at_back(t_list *front_ptr, int elem);
t_bool			list_add_elem_at_position(t_list *front_ptr,
						  int elem,
						  unsigned int position);

t_bool			list_del_elem_at_front(t_list *front_ptr);
t_bool			list_del_elem_at_back(t_list *front_ptr);
t_bool			list_del_elem_at_position(t_list *front_ptr,
						  unsigned int position);

/* Value Access */

int			list_get_elem_at_front(t_list list);
int			list_get_elem_at_back(t_list list);
int			list_get_elem_at_position(t_list list,
						  unsigned int position);

/* Node Access */

t_node			*list_get_first_node_with_sock(t_list list, int value);


#endif			/* !SIMPLE_LIST_H_ */

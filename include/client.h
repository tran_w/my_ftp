/*
** client.h for  in /home/tran_w/Projects/my_ftp/include
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr  8 13:30:16 2013 tran_w
** Last update Sun Apr 14 22:13:55 2013 tran_w
*/

#ifndef			CLIENT_H__
# define		CLIENT_H__

# include		<unistd.h>

# define		R_FAILURE	(-1)
# define		R_SUCCESS	(0)

# define		ERR_SOCKET	"Error: socket() has failed."
# define		ERR_PROTOCOL	"Error: getprotobyname() has failed."
# define		ERR_CONNECT	"Error: connect() has failed."
# define		ERR_INIT	"Error: something went wrong during \
connection."
# define		ERR_SEND	"Error: could not write on socket."
# define		ERR_RECV	"Error: could not read on socket."
# define		ERR_SELECT	"Error: select() has failed."

# define		DISCONNECT_MSG	"DISCONNECT"
# define		GOODBYE_MSG	"Good bye!"
# define		KICKED_MSG	"Connection with server \
has been closed."

# define		BUFFER_SIZE	(1024)
# define		IS_TRUE		(42)

# define		END_OF_PROGRAM	"Exiting program now."
# define		EOL		"\n"
# define		PROTOCOL	"TCP"
# define		PROMPT		"[my_ftp]\n"

# define		CLIENT_USAGE	"./my_client <server's address> <port>"

typedef struct		s_client
{
  int			sock;
  char			*ip;
}			t_client;

size_t			my_recv(int socket, char *buffer, size_t len);
size_t			my_send(int socket, char *buffer, size_t len);
int			init_connection(t_client *client,
					const char *address,
					const int port);
void			usage(void);
int			loop(const int sock);

#endif			/* !CLIENT_H__ */

/*
** server.h for  in /home/tran_w/Projects/my_ftp/include
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr  8 13:29:50 2013 tran_w
** Last update Sun Apr 14 23:06:20 2013 tran_w
*/

#ifndef			SERVER_H__
# define		SERVER_H__

# include		<unistd.h>
# include		<sys/types.h>
# include		"simple_list.h"

# define		R_FAILURE	(-1)
# define		R_SUCCESS	(0)

# define		MAX_CLIENTS	(42)
# define		BUFFER_SIZE	(1024)
# define		IP_SIZE		(256)
# define		NB_CMDS		(4)

# define		ERR_SOCKET	"Error: socket() has failed."
# define		ERR_PROTOCOL	"Error: getprotobyname() has failed."
# define		ERR_INIT	"Error: something went wrong \
during initialisation."
# define		ERR_LISTEN	"Error: listen() has failed"
# define		ERR_BIND	"Error: bind() has failed"
# define		ERR_SEND	"Error: could not write on socket."
# define		ERR_RECV	"Error: could not read on socket."
# define		ERR_SELECT	"Error: select() has failed."
# define		ERR_ACCEPT	"Error: accept() has failed."
# define		ERR_CLOSE	"Error: close() has failed."
# define		ERR_IP		"Error: could not read client ip."
# define		ERR_CMD		"is not a valid command."
# define		ERR_PWD		"couldn't execute pwd."
# define		ERR_LS		"couldn't execute ls."
# define		ERR_CD		"is not a valid directory."
# define		ERR_QUIT	"couldn't quit."

# define		DISCONNECT_MSG	"DISCONNECT"
# define		CMD_MSG		"is executed properly."
# define		GOODBYE_MSG	"[Server] : Good bye!"
# define		CONNECT_MSG	"[Server] : the following IP is \
connected : "

# define		IS_TRUE		(42)
# define		END_OF_PROGRAM	"Exiting program now."
# define		PROTOCOL	"TCP"
# define		SEPARATOR	" "

# define		SERVER_USAGE	"./my_server <port>"

typedef struct		s_commands
{
  char			*name;
  int			(*ptr_func)();
}			t_commands;

size_t			my_recv(int socket, char *buffer, size_t len);
size_t			my_send(int socket, char *buffer, size_t len);
int			init_connection(const int port);
int			loop(const int sock);
int			add_client(t_list *clients, const int const *sock);
int			respond(t_list *clients,
				const fd_set const *rfds,
				const fd_set const *wfds,
				const unsigned int nb_clients);
int			my_pwd(const int sock);
int			my_quit(const int sock);
int			my_ls(const int sock);
int			my_cd(const int sock, const char *path);
void			usage(void);
void			exec_command(const int sock, char *cmd);
char			**my_str_to_wordtab(char *str, char *sep);

#endif			/* !SERVER_H__ */

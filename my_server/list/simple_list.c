/*
** simple_list.c for  in /home/tran_w/Projects/my_ftp/server
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Fri Apr 12 16:39:41 2013 tran_w
** Last update Sun Apr 14 17:35:15 2013 tran_w
*/

#include		<stdio.h>
#include		<stdlib.h>
#include		"simple_list.h"

unsigned int		list_get_size(t_list list)
{
  unsigned int		i;

  i = 0;
  while (list != NULL)
    {
      i++;
      list = list->next;
    }
  return (i);
}

t_bool			list_is_empty(t_list list)
{
  if (list != NULL)
    return (FALSE);
  return (TRUE);
}

void			list_dump(t_list list)
{
  while (list != NULL)
    {
      printf("%d\n", list->sock);
      list = list->next;
    }
}

t_bool			list_add_elem_at_front(t_list *front_ptr, int elem)
{
  t_list		new_elem;

  if ((new_elem = malloc(sizeof(t_node))) == NULL)
    return (FALSE);
  new_elem->sock = elem;
  if ((*front_ptr) == NULL)
    {
      new_elem->next = NULL;
      (*front_ptr) = new_elem;
    }
  else
    {
      new_elem->next = (*front_ptr);
      (*front_ptr) = new_elem;
    }
  return (TRUE);
}

t_bool			list_add_elem_at_back(t_list *front_ptr, int elem)
{
  t_list		new_elem;
  t_list		tmp;

  tmp = (*front_ptr);
  if ((new_elem = malloc(sizeof(t_node))) == NULL)
    return (FALSE);
  new_elem->sock = elem;
  new_elem->next = NULL;
  if (*front_ptr == NULL)
    (*front_ptr) = new_elem;
  else
    {
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = new_elem;
    }
  return (TRUE);
}

t_bool			list_add_elem_at_position(t_list * front_ptr,
						  int elem,
						  unsigned int position)
{
  t_list		new_elem;
  t_list		tmp;
  unsigned int		i;

  tmp = (*front_ptr);
  if ((new_elem = malloc(sizeof(t_node))) == NULL)
    return (FALSE);
  new_elem->sock = elem;
  if (*front_ptr == NULL)
    {
      new_elem->next = NULL;
      (*front_ptr) = new_elem;
    }
  else if (position == 0)
    list_add_elem_at_front(front_ptr, elem);
  else if (position == list_get_size(*front_ptr))
    list_add_elem_at_back(front_ptr, elem);
  else
    {
      for (i = 0; i < position - 1; ++i)
	tmp = tmp->next;
      new_elem->next = tmp->next;
      tmp->next = new_elem;
    }
  return (TRUE);
}

t_bool			list_del_elem_at_back(t_list *front_ptr)
{
  t_list		tmp;

  tmp = (*front_ptr);
  if (tmp == NULL)
    return (FALSE);
  if (tmp->next == NULL)
    {
      free(tmp);
      return (TRUE);
    }
  while (tmp->next->next != NULL)
    tmp = tmp->next;
  free(tmp->next);
  tmp->next = NULL;
  return (TRUE);
}

t_bool			list_del_elem_at_front(t_list *front_ptr)
{
  t_list		tmp;

  tmp = (*front_ptr);
  if (tmp == NULL)
    return (FALSE);
  free(tmp);
  *front_ptr = tmp->next;
  return (TRUE);
}

t_bool			list_del_elem_at_position(t_list *front_ptr,
						  unsigned int position)
{
  t_list		tmp;
  t_list		save;
  unsigned int		i;

  tmp = (*front_ptr);
  if (tmp == NULL)
    return (FALSE);
  else if (position == 0)
    {
      list_del_elem_at_front(front_ptr);
      return (TRUE);
    }
  else if (position == list_get_size(*front_ptr))
    {
      list_del_elem_at_back(front_ptr);
      return (TRUE);
    }
  for (i = 0; i < position - 1; ++i)
    tmp = tmp->next;
  save = tmp->next;
  tmp->next = tmp->next->next;
  free(save);
  return (TRUE);
}

int			list_get_elem_at_front(t_list list)
{
  if (list == NULL)
    return (0);
  return (list->sock);
}
int			list_get_elem_at_back(t_list list)
{
  if (list == NULL)
    return (0);
  while (list->next != NULL)
    list = list->next;
  return (list->sock);
}

int			list_get_elem_at_position(t_list list,
						  unsigned int position)
{
  unsigned int		i;
  unsigned int		position_max;

  i = 0;
  position_max = list_get_size(list);
  if (list == NULL || position >= position_max)
    return (0);
  else if (position == 0)
    list_get_elem_at_front(list);
  else if (position == position_max)
    list_get_elem_at_back(list);
  while (i < position)
    {
      list = list->next;
      i++;
    }
  return (list->sock);
}

t_node			*list_get_first_node_with_sock(t_list list, int sock)
{
  t_list		tmp;

  tmp = list;
  if (list == NULL)
    return (NULL);
  while (tmp != NULL)
    {
      if (tmp->sock == sock)
	return (list);
      tmp = tmp->next;
    }
  return (NULL);
}

/*
** loop.c for  in /home/tran_w/Projects/my_ftp
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Thu Apr 11 16:52:07 2013 tran_w
** Last update Sun Apr 14 22:15:37 2013 tran_w
*/

#include		<sys/select.h>
#include		<sys/types.h>
#include		<sys/time.h>
#include		<unistd.h>
#include		<string.h>
#include		<stdio.h>
#include		"server.h"
#include		"simple_list.h"

static void		set_fds(const int sock,
				fd_set *rfds,
				fd_set *wfds,
				const t_list const *clients)
{
  unsigned int	       	i;
  unsigned int	       	nb_clients;

  FD_ZERO(rfds);
  FD_ZERO(wfds);
  FD_SET(STDIN_FILENO, rfds);
  FD_SET(sock, rfds);
  if (list_is_empty(*clients) == FALSE)
    {
      i = 0;
      nb_clients = list_get_size(*clients);
      while (i <= nb_clients)
	{
	  FD_SET(list_get_elem_at_position(*clients, i), rfds);
	  FD_SET(list_get_elem_at_position(*clients, i), wfds);
	  i++;
	}
    }
}

static int		set_max_fd(const int server,
				   const t_list const *clients)
{
  if (list_get_size(*clients) == 0)
    return (server);
  return (list_get_elem_at_front(*clients));
}

int			loop(const int sock)
{
  fd_set		rfds;
  fd_set		wfds;
  t_list		clients;

  clients = NULL;
  while (IS_TRUE)
    {
      set_fds(sock, &rfds, &wfds, &clients);
      if ((select(set_max_fd(sock, &clients) + 1,
		  &rfds, NULL, NULL, NULL)) == R_FAILURE)
  	return (R_FAILURE);
      if (FD_ISSET(STDIN_FILENO, &rfds))
  	return (R_SUCCESS);
      else if (FD_ISSET(sock, &rfds))
  	add_client(&clients, &sock);
      else
  	{
  	  if (respond(&clients, &rfds, &wfds,
		      list_get_size(clients)) == R_FAILURE)
	    return (R_FAILURE);
  	}
    }
  return (R_SUCCESS);
}

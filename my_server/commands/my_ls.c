/*
** my_ls.c for  in /home/tran_w/Projects/my_ftp/my_server/commands
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Sun Apr 14 21:03:45 2013 tran_w
** Last update Sun Apr 14 22:11:05 2013 tran_w
*/

#include		<sys/types.h>
#include		<sys/stat.h>
#include		<string.h>
#include		<unistd.h>
#include		<stdio.h>
#include		<dirent.h>
#include		"server.h"

int			my_ls(const int sock)
{
  DIR			*my_dir;
  struct dirent		*my_file;
  char			buffer[BUFFER_SIZE];

  my_dir = opendir(".");
  while ((my_file = readdir(my_dir)) != NULL)
    {
      snprintf(buffer, BUFFER_SIZE, "%s\n", my_file->d_name);
      if (!my_send(sock, buffer, strlen(buffer)))
	return (R_FAILURE);
    }
  closedir(my_dir);
  return (R_SUCCESS);
}

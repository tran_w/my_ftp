/*
** my_pwd.c for  in /home/tran_w/Projects/my_ftp/server/commands
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Sun Apr 14 19:41:16 2013 tran_w
** Last update Sun Apr 14 22:53:21 2013 tran_w
*/

#include	<string.h>
#include	<stdio.h>
#include	"server.h"

int		my_pwd(const int sock)
{
  char		buffer[BUFFER_SIZE];

  if (getcwd(buffer, BUFFER_SIZE) == NULL)
    {
      snprintf(buffer, BUFFER_SIZE, "[ERROR] : %s\n", ERR_PWD);
      my_send(sock, buffer, strlen(buffer));
      return (R_FAILURE);
    }
  my_send(sock, buffer, strlen(buffer));
  return (R_SUCCESS);
}

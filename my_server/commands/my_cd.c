/*
** my_cd.c for  in /home/tran_w/Projects/myftp-2015s-2016-2017si-tran_w
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Sun Apr 14 22:51:32 2013 tran_w
** Last update Sun Apr 14 23:41:47 2013 tran_w
*/

#include		<unistd.h>
#include		<string.h>
#include		<stdio.h>
#include		"server.h"

extern char		*g_path;

int			my_cd(const int sock, const char *path)
{
  char			buffer[BUFFER_SIZE];
  char			dir_path[BUFFER_SIZE];

  getcwd(dir_path, BUFFER_SIZE);
  if (path != NULL)
    strcat(dir_path, path);
    if (strncmp(g_path, dir_path, strlen(g_path)) || chdir(path) == R_FAILURE)
    {
      snprintf(buffer, BUFFER_SIZE, "[ERROR] : %s %s\n", path, ERR_CD);
      my_send(sock, buffer, strlen(buffer));
      return (R_FAILURE);
    }
  return (R_SUCCESS);
}

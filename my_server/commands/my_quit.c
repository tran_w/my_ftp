/*
** my_quit.c for  in /home/tran_w/Projects/my_ftp/server/commands
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Sun Apr 14 20:25:54 2013 tran_w
** Last update Sun Apr 14 22:53:32 2013 tran_w
*/

#include	<string.h>
#include	<stdio.h>
#include	"server.h"

int		my_quit(const int sock)
{
  char		buffer[BUFFER_SIZE];

  if (my_send(sock, DISCONNECT_MSG, strlen(DISCONNECT_MSG)))
    {
      snprintf(buffer, BUFFER_SIZE, "[ERROR] : %s\n", ERR_QUIT);
      return (R_FAILURE);
    }
  return (R_SUCCESS);
}

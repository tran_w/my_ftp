/*
** my_send.c for  in /home/tran_w/Projects/my_ftp/server
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr  8 19:40:48 2013 tran_w
** Last update Mon Apr  8 22:26:13 2013 tran_w
*/

#include	<unistd.h>
#include	<stdio.h>
#include	"server.h"

size_t		my_send(int socket, char *buffer, size_t len)
{
  int		byte;

  if ((byte = write(socket, buffer, len)) == R_FAILURE)
    {
      fprintf(stderr, "%s\n", ERR_SEND);
      return (R_FAILURE);
    }
  return (byte);
}

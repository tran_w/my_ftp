/*
** add_client.c for  in /home/tran_w/Projects/my_ftp/server
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Sun Apr 14 16:51:26 2013 tran_w
** Last update Sun Apr 14 22:15:29 2013 tran_w
*/

#include		<netdb.h>
#include		<sys/types.h>
#include		<sys/socket.h>
#include		<stdio.h>
#include		"simple_list.h"
#include		"server.h"

int			add_client(t_list *clients, const int const *sock)
{
  struct sockaddr_in	s_in;
  socklen_t		s_insize;
  char			client_ip[IP_SIZE];
  int			client_sock;

  s_insize = sizeof(s_in);
  if ((client_sock = accept(*sock, (struct sockaddr *)&s_in,
		     &s_insize)) == R_FAILURE)
    {
      fprintf(stderr, "%s\n", ERR_ACCEPT);
      return (R_FAILURE);
    }
  if (!my_recv(client_sock, client_ip, IP_SIZE))
    fprintf(stderr, "%s\n", ERR_IP);
  printf("%s %s\n", CONNECT_MSG, client_ip);
  list_add_elem_at_front(clients, client_sock);
  return (R_SUCCESS);
}

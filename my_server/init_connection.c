/*
** init_connection.c for  in /home/tran_w/Projects/my_ftp/server
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr  8 16:46:40 2013 tran_w
** Last update Sun Apr 14 23:18:06 2013 tran_w
*/

#include		<stdlib.h>
#include		<arpa/inet.h>
#include		<sys/types.h>
#include		<sys/socket.h>
#include		<netdb.h>
#include		<stdio.h>
#include		"server.h"

char			*g_path;

static int		init_socket(int *sock,
				    struct sockaddr_in *s_in,
				    struct protoent *pe,
				    const int port)
{
  if ((*sock = socket(AF_INET,
			       SOCK_STREAM, pe->p_proto)) == R_FAILURE)
    {
      fprintf(stderr, "%s\n", ERR_SOCKET);
      return (R_FAILURE);
    }
  s_in->sin_family = AF_INET;
  s_in->sin_port = htons(port);
  s_in->sin_addr.s_addr = htonl(INADDR_ANY);
  return (R_SUCCESS);
}

static int		init_protocol(struct protoent *pe)
{
  if ((pe = getprotobyname(PROTOCOL)) == NULL)
    {
      fprintf(stderr, "%s\n", ERR_PROTOCOL);
      return (R_FAILURE);
    }
  return (R_SUCCESS);
}

static int		init_path()
{
  if ((g_path = malloc(sizeof(char) * BUFFER_SIZE)) == NULL)
    return (R_FAILURE);
  if ((g_path = getcwd(g_path, BUFFER_SIZE)) == NULL)
    return (R_FAILURE);
  return (R_SUCCESS);
}
int			init_connection(const int port)
{
  struct protoent	pe;
  struct sockaddr_in	s_in;
  int			sock;

  if (init_protocol(&pe))
    return (R_FAILURE);
  if (init_socket(&sock, &s_in, &pe, port))
    return (R_FAILURE);
  if (bind(sock, (const struct sockaddr *)&s_in, sizeof(s_in)))
    {
      fprintf(stderr, "%s\n", ERR_BIND);
      close(sock);
      return (R_FAILURE);
    }
  if (listen(sock, MAX_CLIENTS))
    {
      fprintf(stderr, "%s\n", ERR_LISTEN);
      close(sock);
      return (R_FAILURE);
    }
  if (init_path() == R_FAILURE)
    return (R_FAILURE);
  return (sock);
}

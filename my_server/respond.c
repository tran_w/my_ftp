/*
** respond.c for  in /home/tran_w/Projects/my_ftp/server
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Sun Apr 14 16:50:19 2013 tran_w
** Last update Sun Apr 14 17:58:32 2013 tran_w
*/

#include		<sys/select.h>
#include		<sys/types.h>
#include		<stdio.h>
#include		"simple_list.h"
#include		"server.h"

static int		disconnect_client(int sock,
					  t_list *clients,
					  unsigned int pos)
{
  if (close(sock) == R_FAILURE)
    {
      fprintf(stderr, "%s\n", ERR_CLOSE);
      return (R_FAILURE);
    }
  list_del_elem_at_position(clients, pos);
  return (R_SUCCESS);
}

int			respond(t_list *clients,
				const fd_set const *rfds,
				const fd_set const *wfds,
				const unsigned int nb_clients)
{
  unsigned int		i;
  char			buffer[BUFFER_SIZE];
  int			sock;

  i = -1;
  if (list_is_empty(*clients) == FALSE)
    {
      while (++i <= nb_clients)
	{
	  sock = list_get_elem_at_position(*clients, i);
	  if (FD_ISSET(sock, rfds) && FD_ISSET(sock, wfds))
	    {
	      if (my_recv(sock, buffer, BUFFER_SIZE) <= 0)
		{
		  if (disconnect_client(sock, clients, i) == R_FAILURE)
		    return (R_FAILURE);
		  respond(clients, rfds, wfds, list_get_size(*clients));
		}
	      else
		exec_command(sock, buffer);
	    }
	}
    }
  return (R_SUCCESS);
}

/*
** exec_command.c for  in /home/tran_w/Projects/my_ftp
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Sun Apr 14 19:08:51 2013 tran_w
** Last update Sun Apr 14 23:06:45 2013 tran_w
*/

#include		<stdio.h>
#include		<string.h>
#include		"server.h"

static t_commands	g_cmd[NB_CMDS] =
  {
    {"cd", &my_cd},
    {"quit", &my_quit},
    {"pwd", &my_pwd},
    {"ls", &my_ls}
  };

static int		get_pos_cmd(char *cmd)
{
  int			i;

  i = 0;
  while (i != NB_CMDS)
    {
      if (!strcmp(cmd, g_cmd[i].name))
	return (i);
      i++;
    }
  return (R_FAILURE);
}

void			exec_command(const int sock, char *cmd)
{
  int			pos;
  char			buffer[BUFFER_SIZE];
  char			**wtab;

  wtab = my_str_to_wordtab(cmd, SEPARATOR);
  if ((pos = get_pos_cmd(wtab[0])) == R_FAILURE)
    {
      snprintf(buffer, BUFFER_SIZE, "[ERROR] : %s %s", cmd, ERR_CMD);
      my_send(sock, buffer, strlen(buffer));
      return ;
    }
  if (!strcmp(g_cmd[pos].name, "cd"))
    {
      if (g_cmd[pos].ptr_func(sock, wtab[1]) == R_SUCCESS)
	{
	  snprintf(buffer, BUFFER_SIZE, "[SUCCESS] : %s %s", cmd, CMD_MSG);
	  my_send(sock, buffer, strlen(buffer));
	}
    }
  else if (g_cmd[pos].ptr_func(sock) == R_SUCCESS)
    {
      snprintf(buffer, BUFFER_SIZE, "[SUCCESS] : %s %s", cmd, CMD_MSG);
      my_send(sock, buffer, strlen(buffer));
    }
}

/*
** usage.c for  in /home/tran_w/Projects/my_ftp/server
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr  8 16:25:44 2013 tran_w
** Last update Mon Apr  8 16:33:26 2013 tran_w
*/

#include	<stdio.h>
#include	"server.h"

void		usage(void)
{
  printf("How to use my_server:\n");
  printf("%s\n", SERVER_USAGE);
}

/*
** main.c for  in /home/tran_w/Projects/my_ftp/server
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr  8 14:45:10 2013 tran_w
** Last update Sun Apr 14 14:45:18 2013 tran_w
*/

#include	<stdlib.h>
#include	<stdio.h>
#include	"server.h"

int		main(int ac, char **av)
{
  int		server_socket;

  if (ac != 2)
    {
      usage();
      return (EXIT_FAILURE);
    }
  if ((server_socket = init_connection(atoi(av[1]))) == R_FAILURE)
    {
      fprintf(stderr, "%s\n%s\n", ERR_INIT, END_OF_PROGRAM);
      return (EXIT_FAILURE);
    }
  if (loop(server_socket) == R_FAILURE)
    {
      fprintf(stderr, "%s\n%s\n", ERR_SELECT, END_OF_PROGRAM);
      close(server_socket);
      return (EXIT_FAILURE);
    }
  close(server_socket);
  puts(GOODBYE_MSG);
  return (EXIT_SUCCESS);
}

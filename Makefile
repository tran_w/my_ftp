##
## Makefile for  in /home/tran_w/Projects/my_ftp
## 
## Made by tran_w
## Login   <tran_w@epitech.net>
## 
## Started on  Mon Apr  8 13:09:34 2013 tran_w
## Last update Sun Apr 14 22:35:55 2013 tran_w
##

SERVER_NAME	=	server
CLIENT_NAME	=	client

SERVER_DIR	=	./my_server
CLIENT_DIR	=	./my_client
INCLUDE_DIR	=	./include

SERVER_SRCS	=	$(SERVER_DIR)/main.c \
			$(SERVER_DIR)/usage.c \
			$(SERVER_DIR)/init_connection.c \
			$(SERVER_DIR)/my_send.c \
			$(SERVER_DIR)/my_recv.c \
			$(SERVER_DIR)/loop.c \
			$(SERVER_DIR)/my_str_to_wordtab.c \
			$(SERVER_DIR)/list/simple_list.c \
			$(SERVER_DIR)/respond.c \
			$(SERVER_DIR)/add_client.c \
			$(SERVER_DIR)/exec_command.c \
			$(SERVER_DIR)/commands/my_pwd.c \
			$(SERVER_DIR)/commands/my_quit.c \
			$(SERVER_DIR)/commands/my_ls.c \
			$(SERVER_DIR)/commands/my_cd.c

CLIENT_SRCS	=	$(CLIENT_DIR)/main.c \
			$(CLIENT_DIR)/usage.c \
			$(CLIENT_DIR)/init_connection.c \
			$(CLIENT_DIR)/my_recv.c \
			$(CLIENT_DIR)/my_send.c \
			$(CLIENT_DIR)/loop.c

SERVER_OBJS	=	$(SERVER_SRCS:.c=.o)
CLIENT_OBJS	=	$(CLIENT_SRCS:.c=.o)

CFLAGS		+=	-W -Wall -Wextra -I$(INCLUDE_DIR)

CC		=	gcc

all		:	$(SERVER_NAME) $(CLIENT_NAME)

s		:	$(SERVER_NAME)

c		:	$(CLIENT_NAME)

$(SERVER_NAME)	:	$(SERVER_OBJS)
			$(CC) -o $(SERVER_NAME) $(SERVER_OBJS)

$(CLIENT_NAME)	:	$(CLIENT_OBJS)
			$(CC) -o $(CLIENT_NAME) $(CLIENT_OBJS)

clean		:
			$(RM) $(SERVER_OBJS) $(CLIENT_OBJS)
			$(RM) $(SERVER_DIR)/*~
			$(RM) $(CLIENT_DIR)/*~
			$(RM) $(INCLUDE_DIR)/*~
			$(RM) $(SERVER_DIR)/\#*#
			$(RM) $(INCLUDE_DIR)/\#*#
			$(RM) $(CLIENT_DIR)/\#*#

fclean		:	clean
			$(RM) $(SERVER_NAME) $(CLIENT_NAME)

re		:	fclean all

.PHONY		:	clean fclean re all s c

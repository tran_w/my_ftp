# my_ftp
The purpose of this project is to create an FTP client and server

## Requirements:
To run the program properly the following requirement should be met:

* Linux

* gcc

## How to install
To install:

**make**

## Usage
* For the client side: ./my_client <server's address> <port number>
* For the server side: ./my_server <port number>

## About
This project is realized in C language.